#include "glwidget.h"
#include "GL/glu.h"
#include <iostream>

using namespace std;

GLWidget::GLWidget(QWidget *parent) :
  QGLWidget(parent)
{
}

/*---------------------------------------------------------------------------*
  Configura os recursos e estado do OpenGL. Deve ser chamado uma vez antes da
  primeira vez em que resizeGL() e paintGL() são chamados.
 *---------------------------------------------------------------------------*/
void GLWidget::initializeGL()
{
    parser = new Parser("../recursos/cenarioFinalOpengl.obj");
    //parser = new Parser("../recursos/cubo.obj");
    parser->carregarArquivo();



    float luzAmbiente[4]={0.2,0.2,0.2,1.0};
    // Ativa o uso da luz ambiente
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

    /*********************************************
      Determinando Luz 0, e suas propriedades
     *********************************************/

    /* turn on default lighting */
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    /* set up depth-buffering */
    glEnable(GL_DEPTH_TEST);

    // Intensities (R, G, B) of the ambient, difuse and specular lights
    float light_ambient0[4]  = {0.3, 0.3, 0.3, 1.0};
    float light_diffuse0[4]  = {0.6, 0.6, 0.6, 1.0};
    float light_specular0[4] = {0.9, 0.9, 0.9, 1.0};
    float posicaoDaLuz0[4]   = {0.9, 2.8, 0.4, 1.0};

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient0);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse0 );
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular0 );
    glLightfv(GL_LIGHT0, GL_POSITION, posicaoDaLuz0 );

    float light_ambient1[4]  = {0.1, 0.1, 0.1, 1.0};
    float light_diffuse1[4]  = {0.2, 0.2, 0.2, 1.0};
    float light_specular1[4] = {0.5, 0.5, 0.5, 1.0};
    float posicaoDaLuz1[4]   = {4.79 , 5.5, 6.8, 1.0};

    float posicaoDaLuz2[4]   = {-4.1 , 5.5, 6.8, 1.0};

    // postes
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient1);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse1 );
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular1 );
    glLightfv(GL_LIGHT1, GL_POSITION, posicaoDaLuz1 );

    glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient1);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse1 );
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular1 );
    glLightfv(GL_LIGHT2, GL_POSITION, posicaoDaLuz2 );


    glShadeModel(GL_SMOOTH);

}

/*---------------------------------------------------------------------------*
  Configura o viewport, projeção, etc do OpenGL. Deve ser chamado sempre que
  o widget tem sido redimensionado (e também quando é mostrado pela primeira
  vez devido a todos os widgets criados recentemente obterem um evento de
  redimensionamento automaticamente).
 *---------------------------------------------------------------------------*/
void GLWidget::resizeGL(int w, int h)
{
  glClearColor(0.0,0.0,0.0,0.0);
  glClear(GL_COLOR_BUFFER_BIT);

  if (h == 0) h = 1;
  if (w == 0) w = 1;

  glViewport(0,0,w,h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

}

/*---------------------------------------------------------------------------*
  Renderiza a cena em OpenGL. Deve ser chamado sempre que o widget precisar
  ser atualizado.
 *---------------------------------------------------------------------------*/
void GLWidget::paintGL()
{



  glClearColor(0.0,0.0,0.0,0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  float aspectRatio = 800 / 600;
  gluPerspective(90, aspectRatio, 0.1,500);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // eye(x,y,z) - posição do olho
  // center(x,y,z) - ponto para onde ta olhando
  // up(x,y,z) - vetor up
  gluLookAt(10, 10, 10, 0, 0, 0, 0, 1, 0);


  /********************************************
   Definindo material do objeto
   ********************************************/

  // Coefficients of reflexion (ksr, ksg, ksb), (kdr, kdg, kdb), (kar, kag, kab) of the faces
      float mat_ambient[4] = {0.25f, 0.20f, 0.07f, 1.0f};
      float mat_diffuse[4]   = {0.75f, 0.61f, 0.23f, 1.0f}; // cor
      float mat_specular[4] = {0.63f, 0.56f, 0.37f, 1.0f}; // brilho

      // Exponent representing the shininess of the surface
      float mat_shininess[1]  = {10.2f};

      glMaterialfv(GL_FRONT,GL_SPECULAR, mat_specular);
      glMaterialfv(GL_FRONT,GL_DIFFUSE,mat_diffuse);
      glMaterialfv(GL_FRONT,GL_AMBIENT,mat_ambient);
      glMaterialfv(GL_FRONT,GL_SHININESS,mat_shininess);

  /********************************************
   Printando objeto carregado
    *******************************************/
  vector< Ponto > vertices = parser->vertices;
  vector< Vetor > normais = parser->normais;

  //glPushMatrix();
  //glTranslatef(3.0, 5.0, 0.0);
  //glScalef(3, 3, 3);
  //glRotatef(10, 1,0,0);

   for(int i=0 ; i < vertices.size() ; i+=3){
      glBegin(GL_TRIANGLE_FAN);

              // v1
              glNormal3f(normais[i].x, normais[i].y, normais[i].z);
              glVertex3f(vertices[i].x, vertices[i].y, vertices[i].z);
              // v2
              glNormal3f(normais[i+1].x, normais[i+1].y, normais[i+1].z);
              glVertex3f(vertices[i+1].x, vertices[i+1].y, vertices[i+1].z);
              // v3
              glNormal3f(normais[i+2].x, normais[i+2].y, normais[i+2].z);
              glVertex3f(vertices[i+2].x, vertices[i+2].y, vertices[i+2].z);

             /* Vetor v1v2 = vertices[i] - vertices[i+1];
              Vetor v1v3 = vertices[i] - vertices[i+2];
              Vetor N = produtoVetorial(v1v3, v1v2);
              N = normalizarVetor(N);
              glNormal3f(N.x, N.y, N.z);*/


      glEnd();
      glFlush();
   }


   //glPopMatrix();




}
