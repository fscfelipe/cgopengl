#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "parser.h"

#include <QGLWidget>

class GLWidget : public QGLWidget
{
  Q_OBJECT

public:
  explicit GLWidget(QWidget *parent = 0);

  void initializeGL();
  void paintGL();
  void resizeGL(int w, int h);

  Parser *parser;

private:

public slots:

};

#endif // GLWIDGET_H
