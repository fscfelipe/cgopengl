#ifndef PARSER_H
#define PARSER_H

#include <vector>
#include <ponto.h>
#include <stdio.h>
#include <string.h>
#include <vetor.h>
using namespace std;



class Parser
{
public:
    Parser(const char *path);

    const char *path;

    // Vértices, UVs, Normais dos vertices
    vector< Ponto > vertices, uv;
    vector< Vetor > normais;

    // Variáveis que guardam os índices dos vértices para podermos busca
    // as faces associadas a eles.
    vector< unsigned int > verticeIndices, uvIndices, normalIndices;

    vector< Ponto > tempVertices, tempUVs;
    vector< Vetor > tempNormais;

    bool carregarArquivo();

};

#endif // PARSER_H
